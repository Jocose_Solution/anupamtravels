﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IntInvoiceDetails.aspx.vb" Inherits="IntInvoiceDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
     <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language='javascript'>
        function callprint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'left=0,top=0,width=750,height=500,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write("<html><head><title>Ticket Details</title></head><body>" + prtContent.innerHTML + "</body></html>");
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            //prtContent.innerHTML = strOldOne;
        }
    </script>
    <style>
        .btn-warning {
    color: #fff;
    background-color: #f58220;
    border-color:none;
    border:0px solid #ccc;
    padding:8px;
    border-radius:3px;
}
    </style>
    </head>
<body>
    <form id="form1" runat="server">
    <div id='divprint'>
    <div id="div_invoice" runat="server">
      <asp:Label ID="lbl_IntInvoice" runat="server"></asp:Label>
    </div>
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="900px" style="font-family:Arial; font-size:13px; margin:0 auto; " >
            <tr>
                <td align="center" height="30px">
                     <a href='javascript:;' onclick='javascript:callprint("divprint");'>
                        <img src='../../Images/print_booking.jpg' border='0' /></a>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_PDF" runat="server" CssClass="buttonfltbk" Text="Convert To PDF"
                        CausesValidation="False" Visible="False" />
                    <asp:Button ID="btn_Word" runat="server" CssClass="buttonfltbk" Text="Export To Word"
                        CausesValidation="False" />
                    <asp:Button ID="btn_Excel" runat="server" Text="Export To Excel" CssClass="buttonfltbk"
                        CausesValidation="False" />
                </td>
            </tr>
        </table>
    </div>
    <div style="margin: 10px auto auto 150px; border: 1px #ffffff solid; width: 75%;
        background-color: #FFFFFF; text-align: center">
        <table width="50%" border="0" cellspacing="2" cellpadding="2" bgcolor="#20313f" align="center" style="font-family:Arial; font-size:13px;" >
            <tr>
                <td colspan="2" style="color: #ffffff; font-size: 12px; text-align:left; ">
                    <strong>Send E-Mail:</strong>
                </td>
            </tr>
            <tr>
                <td width="70%" style="color: #ffffff; font-size: 12px">
                    
                    <asp:TextBox ID="txt_email" runat="server" CssClass="textboxflight"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txt_email"
                        ErrorMessage="*" ForeColor="#990000" Display="Dynamic">*</asp:RequiredFieldValidator>
                    <br />
                    <div style="text-align: center; color: #EC2F2F">
                        <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="txt_email"
                            ValidationExpression=".*@.*\..*" ErrorMessage="*Invalid E-Mail ID." Display="dynamic">*Invalid E-Mail ID.</asp:RegularExpressionValidator>
                    </div>
                </td>
                <td width="30%" valign="top">
                    <asp:Button ID="btn" CssClass="buttonfltbk" runat="server" Text="Send"></asp:Button>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="color: #ffffff; font-size: 12px">
                    <asp:Label ID="mailmsg" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
